PROTOC_VERSION ?= 3.19.3

build-all: build-elixir-proto build-go-proto

build-elixir-proto:
	DOCKER_BUILDKIT=1 docker build --build-arg PROTOC_VERSION=$(PROTOC_VERSION) --file dockerfiles/Elixir.Dockerfile --output elixir .

build-go-proto:
	DOCKER_BUILDKIT=1 docker build --build-arg PROTOC_VERSION=$(PROTOC_VERSION) --file dockerfiles/Go.Dockerfile --output go .

