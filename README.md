# Tethys Proto

Easy-button [Google Protocol Buffers](https://developers.google.com/protocol-buffers) for Tethys components.


# Outline
This repository contains protobuf and gRPC defintions that need to be met in order for a Processor to be used
in a Tethys pipeline.
In addition, this document will describe the necessary labels on the Docker image that is produced to be used in Tethys pipelines.

The integration can be broken into two parts:
1. Implement the gRPC Server Interface
2. Meet the Docker Image Requirements

## gRPC Server Interface

This repo is intended to be integrated as a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). To do that, you'll need to run the Git commands shown below. For the examples in this section, it's assumed the submodule files will live at the top-level of the component repo in a `tethys_proto` directory.

```bash
git submodule add git@git.dev.teralogics.com:tethys/proto.git tethys_proto
git submodule init
git submodule update
```

After initial installation, you'll want to update the `tethys_proto` entry in the `.gitmodules` file to look like this:

```
[submodule "tethys_proto"]
    path = tethys_proto
    url = git@git.dev.teralogics.com:tethys/proto.git
    tag = <most recent tag>
```

This repository follows a pattern of trunk-based development which is merged backed into the master branch and then tagged. The intention of that approach is that components will point directly at tags in their submodule config.

> If your component repo resides in the Teralogics GitLab instance, you'll want to update the `url` value to a relative path from where your component repo lives to this repo. For example, if your component lives at the top level of the [Tethys group](https://git.dev.teralogics.com/tethys), your url would look like this:
    
    url = ../proto.git

### Elixir

If you are integrating into an Elixir service, after following the above steps in the [Tethys Component Integration](#tethys-component-integration) section, update/add `elixirc_paths` in `mix.exs` to include the path where you installed the submodule.

```elixir
def project do
    [
      app: :your_app,
      ...
      elixirc_paths: ["lib", "tethys_proto/elixir"],
    ]
  end
```

### Go

If you are integrating into a Go service, after following the steps in the [Tethys Component Integration](#tethys-component-integration) section, import the `tethys` package in `tethys_proto/go`.

```go
import tethys "udp-listener/tethys_proto/go"
```

## Supported Languages

* Elixir
* Go

The repo currently generates code for the above languages via their corresponding [Dockerfiles](dockerfiles/) -- it's easy to expand to other languages! If you want the easy-button integration experience, feel free to submit an MR with a Dockerfile for your language, or reach out to `#cdi-art-blueshift` to collaborate on a solution.

### Rust

This repo doesn't currently generate code for Rust applications. Instead, you can add the [tonic-build](https://crates.io/crates/tonic-build) to the `build-dependencies` section of your `Cargo.toml` to generate code during an app's build stage.

Then add something like the following to your `build.rs` file (assuming the submodule path is `tethys_proto`):

```rust
fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("tethys_proto/proto/tethys.proto")?;
    Ok(())
}
```

## Processor Image Labels
Docker labels enable Tethys to display and validate processor configuration to ensure the Processor works as expected.
More can be read on Docker labels from the [official documentation](https://docs.docker.com/config/labels-custom-metadata/).
A more indepth write up of the labels in the context of Tethys can be read in the [Tethys documentation](https://git.dev.teralogics.com/tethys/documentation).

### `tethys.processor` Namespace
Processor labels must include the `tethys.processor` to be properly parsed. For example, the processor name should be defined as `tethys.processor.name`.

#### Empty Boolean Values
For all boolean values included in the sections below, setting an empty value is equivalent to setting the value to `true`. For example, to indicate a processor supports bidirectional streaming, these two entries produce the same result:

```
tethys.processor.stream=true
tethys.processor.stream=
```

### Required Labels
The labels below are required for a processor to be considered valid.
- `name`
  - `string`
  - The user-friendly processor name. The name should indicate the processor purpose and 
  - Example: `example processor`
- `summary`
  - `string`
  - A general description of the processor, including its intended usage, inputs and outputs.
  - Example: `this is an example processor`
- `type`
  - `string`
  - One of `source`, `processor`, or `sink`.
  - Example: `processor` 

### Optional Labels
The labels below can be optionally included and otherwise will use their default values.
- `tags`
  - `string` of comma-delimited values
  - default: `null`
  - Tags to help group processors and support search and filtering.
  - Example: `foo,bar,baz`
- `stream`
  - `boolean`
  - default: `false`
  - Indicates if a processor supports bidirectional streaming RPC or only unary RPC. All processors are required to support unary RPC. For more information, review the [gRPC documentation](https://grpc.io/docs/what-is-grpc/core-concepts/#rpc-life-cycle).
- `shareable`
  - `boolean`
  - default: `false`
  - Indicates if the processor supports processing multiple pipelines at once.
- `dynamic_updates`
  - `boolean`
  - default: `false`
  - Indicates if the processor supports receiving configuration updates at runtime via a gRPC interface. All processors receive initial configuration via environment variables. If a processor doesn't support dynamic updates, then any applied changes will require the processor to be restarted.

- `config`
  - An `object` containing the configurable attributes of a deployed processor. See [config](#config) below.

## Property Labels
Processor properties are configuration items specific to the processor and determine how it operates. Configuration-driven processors are encouraged to support reusability and enable flexibility within operational scenarios versus requiring code changes.

Tethys will parse and validate properties when processors are created and updated as pipeline components within the platform.

### Property Namespace
Each property must include `tethys.processor.property` in its namespace, followed by the property name. For example, to define a `foo` property, the following namespace must be declared: `tethys.processor.property.foo`.

Additional properties fields will build on the base property namespace as described in the [Optional Property Fields](#optional-property-fields) section below.

### Property Names and Types
Property names and data types are defined when declaring the property namespace. A property defaults to a `string` type unless a different type if provided at declaration. Both of these examples define a property named `foo` of type `string`:


```
tethys.processor.property.foo=
tethys.processor.property.foo=string
```

Names can include letters, numbers, and underscores and, if the property maps to an environment variable, match the lowercase version of the environment variable name. For exanple, the property `foo_bar` will result in a `FOO_BAR` environment variable when the container is deployed.

The data type value must be one of `string`, `number`, `boolean`.

### Optional Property Fields
The following fields are optional and have default values.
- `description`
  - `string`
  - Default: `null`
  - A user-friendly description of the property, indicating what it does and any other useful information.
  - Example: `"This property has an example description."`
- `required`
  - `boolean`
  - Default: `false`
  - Indicates if the property is required for the processor to operate and a value must be provided.
- `default`
  - `any`
  - Default: `null`
  - The default value for the property if a value isn't provided.
  > It's invalid for a property to both be required and provide a default.
- `display_name`
  - `string`
  - Default: `null`
  - The name to be displayed in the Tethys UI, if different than the property name.


### Example
Adding Labels to the end of the Dockerfile of your processor, the end of the Dockerfile might look like this:


```
LABEL tethys.processor.name=mti-processor
LABEL tethys.processor.summary="An example processor which processes MTI data."
LABEL tethys.processor.types=processor
LABEL tethys.processor.tags=example,MTI

LABEL tethys.processor.property.mti_version=
LABEL tethys.processor.property.mti_version.type=number
LABEL tethys.processor.property.mti_version.required=
LABEL tethys.processor.property.mti_version.env=STANAG_4607_ED

LABEL tethys.processor.property.gzip=
LABEL tethys.processor.property.gzip.type=boolean
```


## Contributing

### Code Generation
To generate code for all of the supported languages, run the following command:

```bash
make build-all
``` 
