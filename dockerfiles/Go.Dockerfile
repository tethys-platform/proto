FROM golang:1.17.6 as build

ARG PROTOC_VERSION 
ENV PROTOC_FILE protoc-$PROTOC_VERSION-linux-x86_64.zip

RUN apt-get update
RUN apt-get install -y unzip curl

RUN curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/${PROTOC_FILE} \
    && unzip -o $PROTOC_FILE -d /usr/local bin/protoc \
    && unzip -o $PROTOC_FILE -d /usr/local 'include/*'

RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26 \
    && go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1

RUN mkdir -p /output/go

COPY proto/ proto/

WORKDIR proto

RUN protoc --go_out=/output/go --go-grpc_out=/output/go tethys.proto

FROM scratch as export

# copy out the generated proto code
COPY --from=build /output/go/* .
