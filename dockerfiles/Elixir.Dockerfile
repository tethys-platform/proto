FROM artifactory.hq.teralogics.com/cdi/base-images/elixir:1.16.3-erlang-26.2.5-ubi-9.2 as build

ARG PROTOC_VERSION



#RUN yum install -y autoconf automake libtool curl make g++ unzip protobuf 
RUN yum install -y make unzip 
RUN  PB_REL="https://github.com/protocolbuffers/protobuf/releases" && \
	curl -LO $PB_REL/download/v25.1/protoc-25.1-linux-x86_64.zip && \
	unzip protoc-25.1-linux-x86_64.zip -d $HOME/.local

ENV PATH="$PATH:$HOME/.local/bin"

# install protoc
# 3.14.0
RUN PROTOC_ZIP=protoc-${PROTOC_VERSION}-linux-x86_64.zip && \
	curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/$PROTOC_ZIP && \
	unzip -o $PROTOC_ZIP -d /usr/local bin/protoc && \
	unzip -o $PROTOC_ZIP -d /usr/local 'include/*'&& \
	rm -f $PROTOC_ZIP 

RUN mix local.hex --force && mix local.rebar --force
# Install elixir lib for building protobuf files in elixir
RUN mix escript.install --force hex protobuf
ENV PATH=$PATH:/root/.mix/escripts

COPY proto/ proto

WORKDIR proto

RUN mkdir -p /output/elixir

RUN protoc --elixir_out=plugins=grpc:/output/elixir *.proto

FROM scratch as export

COPY --from=build /output/elixir/* .
