defmodule Tethys.ProcessDataResponse.ProcessResult do
  @moduledoc false

  use Protobuf, enum: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :UNKNOWN, 0
  field :PROCESSED, 1
  field :PROCESSED_PREVIOUS, 2
  field :CONTINUE, 3
  field :DROP, 4
  field :ERROR, 5
end

defmodule Tethys.ProducerRequest do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  oneof :request_type, 0

  field :producer_config, 1, type: Tethys.ProducerConfig, json_name: "producerConfig", oneof: 0

  field :producer_disconnect, 2,
    type: Tethys.ProducerDisconnect,
    json_name: "producerDisconnect",
    oneof: 0
end

defmodule Tethys.ProducerResponse do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  oneof :response_type, 0

  field :producer_error, 1, type: Tethys.ProducerError, json_name: "producerError", oneof: 0
  field :producer_data, 2, type: Tethys.ProducerData, json_name: "producerData", oneof: 0
end

defmodule Tethys.ProducerError do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  oneof :failed_request, 0

  field :message, 1, type: :string
  field :producer_config, 2, type: Tethys.ProducerConfig, json_name: "producerConfig", oneof: 0
end

defmodule Tethys.ProducerData.AttributesEntry do
  @moduledoc false

  use Protobuf, map: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :key, 1, type: :string
  field :value, 2, type: :bytes
end

defmodule Tethys.ProducerData do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :id, 1, type: :uint64
  field :data_files, 2, repeated: true, type: Tethys.ProduceDataFile, json_name: "dataFiles"
  field :attributes, 3, repeated: true, type: Tethys.ProducerData.AttributesEntry, map: true
end

defmodule Tethys.ProducerConfig do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :id, 1, type: :uint64
  field :json_request_config, 2, type: :string, json_name: "jsonRequestConfig"
end

defmodule Tethys.ProducerDisconnect do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"
end

defmodule Tethys.ProduceDataFile.AttributesEntry do
  @moduledoc false

  use Protobuf, map: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :key, 1, type: :string
  field :value, 2, type: :bytes
end

defmodule Tethys.ProduceDataFile do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :content, 1, type: :bytes
  field :attributes, 2, repeated: true, type: Tethys.ProduceDataFile.AttributesEntry, map: true
  field :origin_time, 3, type: :int64, json_name: "originTime"
end

defmodule Tethys.ProcessDataRequest.AttributesEntry do
  @moduledoc false

  use Protobuf, map: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :key, 1, type: :string
  field :value, 2, type: :bytes
end

defmodule Tethys.ProcessDataRequest do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :id, 1, type: :uint64
  field :content, 2, type: :bytes
  field :attributes, 3, repeated: true, type: Tethys.ProcessDataRequest.AttributesEntry, map: true
end

defmodule Tethys.ProcessDataResponse.AttributesEntry do
  @moduledoc false

  use Protobuf, map: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :key, 1, type: :string
  field :value, 2, type: :bytes
end

defmodule Tethys.ProcessDataResponse do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :request_id, 1, type: :uint64, json_name: "requestId"
  field :data_files, 2, repeated: true, type: Tethys.ProcessDataFile, json_name: "dataFiles"

  field :attributes, 3,
    repeated: true,
    type: Tethys.ProcessDataResponse.AttributesEntry,
    map: true

  field :result, 4, type: Tethys.ProcessDataResponse.ProcessResult, enum: true
end

defmodule Tethys.ProcessDataFile.AttributesEntry do
  @moduledoc false

  use Protobuf, map: true, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :key, 1, type: :string
  field :value, 2, type: :bytes
end

defmodule Tethys.ProcessDataFile do
  @moduledoc false

  use Protobuf, syntax: :proto3, protoc_gen_elixir_version: "0.12.0"

  field :content, 1, type: :bytes
  field :attributes, 2, repeated: true, type: Tethys.ProcessDataFile.AttributesEntry, map: true
end

defmodule Tethys.Producer.Service do
  @moduledoc false

  use GRPC.Service, name: "tethys.Producer", protoc_gen_elixir_version: "0.12.0"

  rpc :ProduceData, Tethys.ProducerRequest, Tethys.ProducerResponse

  rpc :ProduceDataStream, stream(Tethys.ProducerRequest), stream(Tethys.ProducerResponse)
end

defmodule Tethys.Producer.Stub do
  @moduledoc false

  use GRPC.Stub, service: Tethys.Producer.Service
end

defmodule Tethys.Processor.Service do
  @moduledoc false

  use GRPC.Service, name: "tethys.Processor", protoc_gen_elixir_version: "0.12.0"

  rpc :ProcessData, Tethys.ProcessDataRequest, Tethys.ProcessDataResponse

  rpc :ProcessDataStream, stream(Tethys.ProcessDataRequest), stream(Tethys.ProcessDataResponse)
end

defmodule Tethys.Processor.Stub do
  @moduledoc false

  use GRPC.Stub, service: Tethys.Processor.Service
end